use phpsniffer::PhpsnifferJson;

mod phpsniffer;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = std::env::args().into_iter().collect::<Vec<String>>();
    match args.get(1) {
        Some(filepath) => {
            let file_content = std::fs::read_to_string(filepath)?;
            let phpcs = serde_json::from_str::<PhpsnifferJson>(file_content.as_str())?;
            phpcs.print_analysis();
            Ok(())
        }
        None => Err("File path to scan required!".into()),
    }
}
