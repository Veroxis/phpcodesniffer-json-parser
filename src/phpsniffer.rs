use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct PhpsnifferJson {
    #[serde(rename = "historyAnalyser")]
    history_analyser: String,
    #[serde(rename = "historyMarker")]
    history_marker: String,
    #[serde(rename = "resultsParser")]
    results_parser: String,
    #[serde(rename = "analysisResults")]
    analysis_results: Vec<AnalysisResult>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnalysisResult {
    #[serde(rename = "lineNumber")]
    line_number: u32,
    #[serde(rename = "fileName")]
    file_name: String,
    #[serde(rename = "type")]
    phpsniffer_type: String,
    #[serde(rename = "message")]
    message: String,
}

impl PhpsnifferJson {
    pub fn print_analysis(&self) {
        let mut type_to_count_map: HashMap<&str, u32> = HashMap::new();
        self.analysis_results.iter().for_each(|analysis_result| {
            let counter = type_to_count_map
                .entry(analysis_result.phpsniffer_type.as_str())
                .or_insert_with(|| 0);
            *counter += 1;
        });
        let mut largest_key = u32::MIN;
        type_to_count_map.iter().for_each(|(_, amount)| {
            if *amount > largest_key {
                largest_key = *amount;
            }
        });
        let largest_key_string = largest_key.to_string();
        let largest_key_strlen = largest_key_string.len();
        for idx in 0..=largest_key {
            type_to_count_map.iter().for_each(|(error_type, amount)| {
                if *amount == idx {
                    println!("{amount:>largest_key_strlen$} {error_type}");
                }
            });
        }
        println!();
        println!("Amount total errors:       {}", self.analysis_results.len());
        println!("Amount unique error types: {}", type_to_count_map.len());
    }
}
